import { Schema, model } from 'mongoose'

/*
Define the schema of the User Model

email: string, be unique, without spaces, is mandatory and should be lowercase
password: the select false meaning the query not return this field
firstName: string, it is mandatory
lastName: string, it is mandatory

*/

const UserSchema = new Schema({
  email: { type: String, lowercase: true, trim: true, required: true, unique: true },
  password: { type: String, required: true, select: false },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
})

export default model('User', UserSchema)
