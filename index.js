import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'
import { validateParams } from './validate'

const dbConnectionURI = 'mongodb+srv://test-user:17L2WY9bUWXqQmn3bnfPg8lB@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority'

connect( 
  dbConnectionURI,
  { 
    useNewUrlParser: true, 
    useFindAndModify: false
  })

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', async (req, res) => {
  res.send('Just a test')
})

app.get('/users', async (req, res, next) => {
  try {
    const results = await UserModel.find()
    res.send(results)
  } catch (err){
    next(err)
  }
})

// implment /user pagination 
app.get('/users/:limit/:skip', async (req, res, next) => {
  try {
    const limit = req.params.limit ? parseInt(req.params.limit) : 100
    const skip = req.params.skip ? parseInt(req.params.skip) : 0
    const results = await UserModel.find().sort('_id').skip(skip).limit(limit)
    res.send(results)
  } catch (err){
    next(err)
  }
})

app.post('/users', validateParams( (() => {
            const requestParams = []
            UserModel.schema.eachPath( (path) => {
              requestParams.push({
                param_key: path,
                type: UserModel.schema.path(path).instance.toLowerCase(),
                required: UserModel.schema.path(path).isRequired
              })
            })
            return requestParams
        })()), async (req, res, next) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  try {
    const newUser = await user.save()
    res.send(newUser)
  } catch (err) {
    next(err)
  }
})

// set a /users PUT route that allow to update user data by id 
app.put('/users/:id', async (req, res, next) => { 
  try {
    const newDoc = await UserModel.findOneAndUpdate(
      {
        _id: req.params.id
      },
      req.body,
      {
        returnOriginal: false
      }
      )
    res.send(newDoc)
  } catch (err) {
    next(err)
  }
})

// set a /users DELETE route that allow to delete a user by id
app.delete('/users/:id', async (req, res, next) => {
  try {
    const deletedUser = await UserModel.findOneAndDelete({
      _id: req.params.id
    })
    res.send(deletedUser)
  } catch (err) {
    next(err)
  }
});

// handle request error
app.use((err, req, res, next) => {
  res.status(err.status || 500).send(err.message || 'Internal Server Error')
});


app.listen(8080, () => console.log('Example app listening on port 8080!'))
