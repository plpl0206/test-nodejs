
/*
ValidateParams is middleware before handling request in endpoint

1. check present param whether match the schema field type
2. check non present param whether is required

*/

const validateParams = (requestParams) => {
    return (req, res, next) => {
        for (let param of requestParams) {
            if (checkParamPresent(Object.keys(req.body), param)) {
                let reqParam = req.body[param.param_key];
                if (!checkParamType(reqParam, param)) {
                    return res.send(400, {
                        status: 400,
                        result: `${param.param_key} is of type ` +
                        `${typeof reqParam} but should be ${param.type}`
                    });
                } 
            } else if (param.required){
                return res.send(400, {
                    status: 400,
                    result: `Missing Parameter ${param.param_key}`
                });
            }
        }
        next();
    }
};

const checkParamPresent = (reqParams, paramObj) => {
    return (reqParams.includes(paramObj.param_key));
};

const checkParamType = (reqParam, paramObj) => {
    const reqParamType = typeof reqParam;
    return reqParamType === paramObj.type;
};

export { validateParams }